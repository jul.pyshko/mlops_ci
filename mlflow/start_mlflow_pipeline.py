import logging
import json
import sys
import os

work_dir = os.getcwd()
sys.path.insert(0, work_dir)
print(sys.path)
print(work_dir)

from src.data import *
from src.models import *

from prophet import serialize
from prophet.diagnostics import cross_validation, performance_metrics

import mlflow
from mlflow.models.signature import infer_signature

# run mlflow tracking server locally
# mlflow.set_tracking_uri("http://127.0.0.1:5000")

# run mlflow tracking server remotetly
mlflow.set_tracking_uri("http://192.168.56.101:5000")

mlflow.set_experiment("disk_metrics_forecasting_v.1")


def extract_params(pr_model):
    return {attr: getattr(pr_model, attr) for attr in serialize.SIMPLE_ATTRIBUTES}


def get_metrics(model):
    metric_keys = ["mse", "rmse", "mae", "mape", "mdape", "smape", "coverage"]

    metrics_raw = cross_validation(
        model=model,
        horizon="5 days",
        period="3 days",
        initial="5 days",
        parallel="threads",
        disable_tqdm=True,
    )

    cv_metrics = performance_metrics(metrics_raw)
    metrics = {k: cv_metrics[k].mean() for k in metric_keys}

    return metrics


def pipeline_start():
    input_data_path = "data/raw/host_metrics_ts.csv"
    processed_data_path = "data/processed/mlflow__prepared_ts.csv"
    model_path = "models/mlflow__prophet_model.pkl"
    result_path = "results/mlflow__prediction_result.csv"
    error_path = "results/mlflow__prediction_error.csv"

    run = mlflow.end_run()
    print(f"The current run is stopped: {run}")

    with mlflow.start_run():
        for aggreg_by, prediction_size in params.forecasting_params.items():
            data.prepare_dataset(
                in_filepath=input_data_path,
                out_filepath=processed_data_path,
                csvColName=params.csv_column,
                aggreg_by=aggreg_by,
            )

            model = fm.train(
                input_data_filepath=processed_data_path,
                output_model_filepath=model_path,
                prediction_size=prediction_size,
                freq=aggreg_by,
                daily_seasonality=True,
            )

            predictions_df, errors_dict = fm.predict(
                input_model_filepath=model_path,
                input_data_filepath=processed_data_path,
                output_result_filepath=result_path,
                csvColName=params.csv_column,
                prediction_size=prediction_size,
                aggreg_by=aggreg_by,
                output_error_filepath=error_path,
            )

            # mlflow metrtics log
            model_metrics = get_metrics(model=model)
            mlflow.log_metrics(model_metrics)
            print(f"Logged Metrics: \n{json.dumps(model_metrics, indent=2)}")

            # mlflow parameters log
            model_params = extract_params(pr_model=model)
            mlflow.log_params(model_params)
            print(f"Logged Params: \n{json.dumps(model_params, indent=2)}")

            # mlflow signature
            train_df = model.history
            signature = infer_signature(train_df, predictions_df)

            train_df.to_csv(f"{model_path}_train_df.csv")
            train_df.to_json(f"{model_path}_train_df.json")

            # mlflow model log
            input_example = {
                "ds": 1619827200000,
                "y": 39215614463.96875,
                "floor": 0,
                "t": 0,
                "y_scaled": 0.9812392705,
            }

            artifacts_uri = mlflow.get_artifact_uri()
            print(f"Artifacts are logged to: {artifacts_uri}")
            mlflow.prophet.log_model(
                model,
                signature=signature,
                artifact_path="model",
                registered_model_name="forecasting_model",
                input_example=input_example,
            )

    model_version = "1"
    loaded_model = mlflow.prophet.load_model(
        f"models:/forecasting_model/{model_version}"
    )

    return loaded_model


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    pipeline_start()
