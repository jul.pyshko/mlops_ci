import click
import logging

from src.models import forecast_model as fm
from src.data import forecast_params as params


@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("output_filepath", type=click.Path())
@click.argument("daily_seasonality", default=False, type=click.BOOL)
def train(input_filepath, output_filepath, daily_seasonality):
    """Runs model training scripts based on pre-processed dataset (saved in ../processed)."""
    logger = logging.getLogger(__name__)
    logger.info(f"*** Training model based on dataset from {input_filepath}")

    for aggreg_by, prediction_size in params.forecasting_params.items():
        model = fm.train(
            input_data_filepath=input_filepath,
            output_model_filepath=output_filepath,
            prediction_size=prediction_size,
            freq=aggreg_by,
            daily_seasonality=daily_seasonality,
        )

    logger.info(f"*** Trained model saved successfully into {output_filepath}")

    return model


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    train()
