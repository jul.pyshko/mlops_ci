import click
import logging

from src.models import forecast_model as fm
from src.data import forecast_params as params


@click.command()
@click.argument("input_model_filepath", type=click.Path(exists=True))
@click.argument("input_data_filepath", default="", type=click.Path())
@click.argument("output_result_filepath", type=click.Path())
@click.argument("output_error_filepath", type=click.Path())
def predict(
    input_model_filepath,
    input_data_filepath,
    output_result_filepath,
    output_error_filepath,
):
    """Runs model training scripts based on pre-processed dataset (saved in ../processed)."""
    logger = logging.getLogger(__name__)
    logger.info(f"*** Runs prediction based on model from {input_model_filepath}")

    for aggreg_by, prediction_size in params.forecasting_params.items():
        forecasted_df, errors_dict = fm.predict(
            input_model_filepath=input_model_filepath,
            input_data_filepath=input_data_filepath,
            output_result_filepath=output_result_filepath,
            csvColName=params.csv_column,
            prediction_size=prediction_size,
            aggreg_by=aggreg_by,
            output_error_filepath=output_error_filepath,
        )

    logger.info(
        f"*** Prediction result ans errors are saved successfully into {[output_result_filepath, output_error_filepath]}"
    )

    return forecasted_df, errors_dict


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    predict()
