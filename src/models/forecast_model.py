# import forecast_logging as flog
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from prophet import Prophet
import pickle
import csv


def prepare_model(df: pd.DataFrame, prediction_size: int) -> pd.DataFrame:
    """Prepares training dataset based on prediction size in days

    Args:
        df (pd.DataFrame): input dataset
        prediction_size (int): prediction size in days

    Returns:
        _type_: dataset as pandas Dataframe
    """
    train_df = df[:-prediction_size]

    return train_df


def train_model(train_df: pd.DataFrame, daily_seasonality: bool = False) -> Prophet:
    model = Prophet(uncertainty_samples=100, daily_seasonality=daily_seasonality)
    """Trains model on traning dataset

    Returns:
        _type_: Prophet model object
    """

    # if not isDebug:
    #     with flog.suppress_stdout_stderr():
    #         model.fit(train_df)
    # else:
    model.fit(train_df)

    return model


def prepare_model_plot(
    metric_name: str,
    aggreag_by: str,
    original_df: pd.DataFrame,
    prediction_size: int = None,
) -> list:
    """Prepares model plot

    Args:
        metric_name (str): _description_
        aggreag_by (str): _description_
        original_df (pd.DataFrame): _description_
        prediction_size (int, optional): _description_. Defaults to None.

    Returns:
        list: _description_
    """
    fig = plt.figure(facecolor="w", figsize=(10, 6))
    fig.suptitle(
        "Forecasting chart for metric ["
        + metric_name
        + '] aggregated by "'
        + aggreag_by
        + '"',
        fontsize=14,
    )
    ax = fig.add_subplot(111)

    if prediction_size is None:
        original_points = original_df.copy()
    else:
        shift = -prediction_size - 1
        original_points = (original_df.copy())[shift:]

    ax.plot(
        original_points["ds"].dt.to_pydatetime(),
        original_points["y"],
        "orange",
        label="Observed data points",
    )

    return ax


def make_prediction(
    model,  #: Prophet.forecaster.Prophet
    prediction_size: int,
    metric_name: str,
    original_df: pd.DataFrame,
    freq: str,
):
    """Makes prediction based on prophet algorithm

    Args:
        model (Prophet.forecaster.Prophet): _description_
        prediction_size (int): _description_
        metric_name (str): _description_
        original_df (pd.DataFrame): _description_
        freq (str): _description_
        isDebug (bool, optional): _description_. Defaults to False.

    Returns:
        _type_: _description_
    """
    future_df = model.make_future_dataframe(periods=prediction_size, freq=freq)

    forecast_df = model.predict(future_df)

    # ax = prepare_model_plot(metric_name, freq, original_df)
    # model.plot(forecast_df, ax)

    # print("Forecast df:")
    # print(forecast_df.tail())
    # model.plot_components(forecast_df)

    df_compare = make_comparison_dataframe(original_df, forecast_df)
    errors_dict = calculate_forecast_errors(df_compare, prediction_size)

    for err_name, err_value in errors_dict.items():
        print(err_name, err_value)

    return forecast_df, errors_dict


def make_comparison_dataframe(historical, forecast):
    """Join the history with the forecast.

    The resulting dataset will contain columns 'yhat', 'yhat_lower', 'yhat_upper' and 'y'.
    """

    timeName = "ds"

    return forecast.set_index(timeName)[["yhat", "yhat_lower", "yhat_upper"]].join(
        historical.set_index(timeName)
    )


def calculate_forecast_errors(df, prediction_size):
    """Calculate MAPE and MAE of the forecast.

    Args:
        df: joined dataset with 'y' and 'yhat' columns.
        prediction_size: number of days at the end to predict.
    """
    yName = "y"

    # Make a copy
    df = df.copy()

    # Now we calculate the values of e_i and p_i according to the formulas given in the article above.
    df["e"] = df[yName] - df["yhat"]
    df["p"] = 100 * df["e"] / df[yName]

    # Recall that we held out the values of the last `prediction_size` days
    # in order to predict them and measure the quality of the model.

    # Now cut out the part of the data which we made our prediction for.
    predicted_part = df[-prediction_size:]

    # Define the function that averages absolute error values over the predicted part.
    error_mean = lambda error_name: np.mean(np.abs(predicted_part[error_name]))

    # Now we can calculate MAPE and MAE and return the resulting dictionary of errors.
    return {"MAPE": error_mean("p"), "MAE": error_mean("e")}


def train(
    input_data_filepath: str,
    output_model_filepath: str,
    prediction_size: int,
    freq: str,
    daily_seasonality: bool = False,
):
    """Run forecasting flow

    Args:
        filepath (str): _description_
        csvColName (str): _description_
        prediction_size (int): _description_
        aggreg_by (str, optional): _description_. Defaults to "".
        daily_seasonality (bool, optional): _description_. Defaults to False.
        isDebug (bool, optional): _description_. Defaults to False.

    Returns:
        _type_: _description_
    """

    df = pd.read_csv(input_data_filepath)
    print(df.head())

    train_df = prepare_model(df, prediction_size)
    model = train_model(train_df, daily_seasonality)

    future_df = model.make_future_dataframe(periods=prediction_size, freq=freq)
    print("future ds is prepared for prefiction")
    print(future_df.head())

    future_df.to_csv(f"{output_model_filepath}_future_ds.csv")
    future_df.to_json(f"{output_model_filepath}_future_ds.json")

    print(f"Model will be saved to {output_model_filepath}")

    pickle_model(filename=output_model_filepath, model=model)

    return model


def predict(
    input_model_filepath: str,
    input_data_filepath: str,
    output_result_filepath: str,
    csvColName: str,
    prediction_size: int,
    aggreg_by: str = "",
    output_error_filepath: str = "",
):
    """Run forecasting flow

    Args:
        filepath (str): _description_
        csvColName (str): _description_
        prediction_size (int): _description_
        aggreg_by (str, optional): _description_. Defaults to "".
        daily_seasonality (bool, optional): _description_. Defaults to False.
        isDebug (bool, optional): _description_. Defaults to False.

    Returns:
        _type_: _description_
    """

    original_df = pd.read_csv(input_data_filepath)
    model = load_model(filename=input_model_filepath)

    forecasted_df, errors_dict = make_prediction(
        model, prediction_size, csvColName, original_df, aggreg_by
    )
    print(forecasted_df.head())

    forecasted_df.to_csv(output_result_filepath)
    write_dict(output_error_filepath, errors_dict)

    return forecasted_df, errors_dict


def pickle_model(filename: str, model: object):
    with open(filename, "wb") as f:
        pickle.dump(model, f)


def load_model(filename: str):
    with open(filename, "rb") as f:
        model = pickle.load(f)

    return model


def write_dict(filename: str, values: dict):
    with open(filename, "w") as file:
        writer = csv.writer(file)
        writer.writerows(values)
