# forecasting requirements
# csv_column = "'/' Disk/FreeBytes"
# csv_column = "'/apps' Disk/FreeBytes"
csv_column = "'/logs' Disk/FreeBytes"

# forecasting parameters for research
n_days = 5

forecasting_params = {
    "D": n_days,
    # "H": 24 * n_days,
    # "30min": 48 * n_days,
    # "15min": 96 * n_days,
}
