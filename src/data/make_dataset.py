import click
import logging

from src.data import *


@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("output_filepath", type=click.Path())
def prepare(input_filepath, output_filepath):
    """Runs data processing scripts to turn raw data from (../raw) into
    cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info("*** Making final data set from raw data {input_filepath}")

    outs = []
    for aggreg_by in params.forecasting_params.keys():
        logger.info("Dataset is aggregated by '" + aggreg_by + "'")
        out_path = generate_out_filepath(pattern=output_filepath, var_part=aggreg_by)
        df = data.prepare_dataset(
            in_filepath=input_filepath,
            out_filepath=out_path,
            csvColName=params.csv_column,
            aggreg_by=aggreg_by,
        )
        outs.append(out_path)

    logger.info(f"*** Final data set is pre-processed and saved successfully to {outs}")

    return df


def generate_out_filepath(pattern: str, var_part: str) -> str:
    delimeter = "."
    parts = pattern.split(".")
    ext = parts[len(parts) - 1]
    out_filepath = f"{delimeter.join(parts[:-1])}_{var_part}.{ext}"

    return out_filepath


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    prepare()
