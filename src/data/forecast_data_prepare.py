import pandas as pd


def read_data(
    filepath: str, csvColName: str, timeCol: str = "ds", outCol: str = "y"
) -> pd.DataFrame:
    """Reads data from .csv file and create dataset sorted by timestamp column

    Args:
        filepath (str): path to data file
        csvColName (str): output column name

    Returns:
        pd.DataFrame: output dataset
    """
    df = pd.read_csv(filepath)

    df.rename(columns={csvColName: outCol, "timestamp": timeCol}, inplace=True)
    df = df[[timeCol, outCol]]
    df[timeCol] = pd.to_datetime(df[timeCol])
    df.sort_values(by=timeCol)

    return df


def aggregate_by(
    df: pd.DataFrame, by: str, timeCol: str = "ds", outCol: str = "y"
) -> pd.DataFrame:
    """Aggregates dataset by specific column

    Args:
        df (pd.DataFrame): input dataset
        by (str): column name used for sggregation

    Returns:
        pd.DataFrame: output aggregated dataset
    """
    df = df.groupby(timeCol)[[outCol]].mean()
    df = df.resample(by).mean()
    df = df.reset_index()

    return df


def prepare_dataset(
    in_filepath: str, out_filepath: str, csvColName: str, aggreg_by: str = ""
):
    df = read_data(in_filepath, csvColName)
    print(df.head())

    df_proccessed = aggregate_by(df, aggreg_by)
    print(df_proccessed.head())

    df_proccessed.to_csv(out_filepath)

    return df_proccessed
