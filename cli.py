"""
Entry point for the CLI
"""

import click
import os
import sys
import logging

work_dir = os.getcwd()
sys.path.insert(0, work_dir)
print(sys.path)

from src import prepare
from src import train
from src import predict

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s %(message)s", datefmt="%Y-%m-%d %H:%M:%S"
)


@click.group()
@click.version_option(prog_name="MLOPS CLI")
def cli():
    pass


"""
Register commands into cli group
"""
cli.add_command(prepare)
cli.add_command(train)
cli.add_command(predict)

if __name__ == "__main__":
    cli()
